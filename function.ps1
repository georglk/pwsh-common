#!/usr/bin/env pwsh

function New-TemporaryFile {
	Join-Path -Path $([System.IO.Path]::GetTempPath()) -ChildPath $([System.IO.Path]::GetRandomFileName())
}

function Write-StdErr {
<#
	.SYNOPSIS
		Writes text to stderr when running in a regular console window,
		to the hosts error stream otherwise.

	.DESCRIPTION
		Writing to true stderr allows you to write a well-behaved CLI
		as a PS script that can be invoked from a batch file, for instance.

	Note that PS by default sends ALL its streams to *stdout* when invoked from cmd.exe.

	This function acts similarly to Write-Host in that it simply calls
	.ToString() on its input; to get the default output format, invoke
	it via a pipeline and precede with Out-String.
#>

	param ([PSObject] $InputObject)

	$outFunc = if ($Host.Name -eq 'ConsoleHost') {
		[Console]::Error.WriteLine
	} else {
		$Host.UI.WriteErrorLine
	}

	if ($InputObject) {
		[void] $outFunc.Invoke($InputObject.ToString())
	} else {
		[string[]] $lines = @()
		$Input | ForEach-Object { $lines += $_.ToString() }
		[void] $outFunc.Invoke($lines -join [Environment]::NewLine)
	}
}

function Quit {
	param ([int] $exitcode)

	if ($exitcode) {
		exit $exitcode
	} else {
		exit
	}
}

function Join-Rows {
	param (
		[parameter(Mandatory = $true, Position = 0)]
		[ref]
		$var,

		[parameter(Mandatory = $true, Position = 1)]
		[string[]]
		$text,

		[Alias('n', 'nl')]
		[switch]
		$newline,

		[Alias('s', 'sp')]
		[switch]
		$space
	)

	$OFS = [Environment]::NewLine

	if ($newline) {
		if ($space) {
			$var.Value = $var.Value + $OFS + ' ' + "$text"
		} else {
			$var.Value = $var.Value + $OFS + "$text"
		}
	} else {
		if ($space) {
			$var.Value = $var.Value + ' ' + "$text"
		} else {
			$var.Value = $var.Value + "$text"
		}
	}
}

function Add-Space {
	param (
		[parameter(Mandatory = $true)]
		[string[]]$text
	)

	if (($text) -and ($text.Length -ge 1)) {
		return (' ' + $text)
	}
}

function Show-ErrorObject {
	param (
		[parameter(Mandatory = $true)]
		$private:object,
		[switch]
		$r
	)

	New-Variable -Name line -Force -ErrorAction SilentlyContinue
	Join-Rows ([ref]$line) "Error"
	if ($object.InvocationInfo.MyCommand) {
		Join-Rows ([ref]$line) $(Add-Space $object.InvocationInfo.MyCommand.ToString())
	}
	Join-Rows ([ref]$line) $(Add-Space ":")
	if ($object.Exception.Message) {
		Join-Rows ([ref]$line) $(Add-Space $object.Exception.Message.ToString().Trim())
	}
	if ($object.InvocationInfo.ScriptLineNumber) {
		Join-Rows ([ref]$line) $(Add-Space "line")
		Join-Rows ([ref]$line) $(Add-Space $object.InvocationInfo.ScriptLineNumber.ToString())
	}

	if ($r) {
		return $line
	} else {
		Write-StdErr $line
	}
}

function Test-IsAdmin {
	([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')
}

function Elapsed {
	[CmdletBinding()]
	param (
		[Parameter()]
		[switch]
		$stop
	)

	process {
		if ($stop) {
			$Script:watch.Stop()
			return $Script:watch.Elapsed.ToString()
		} else {
			$Script:watch = [System.Diagnostics.Stopwatch]::StartNew()
		}
	}

	end {
		if ($stop) {
			Remove-Variable -Name watch -Scope Script -Force
		}
	}
}

function Join-Words {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string[]]
		$words,
		[Parameter(Position = 1)]
		[string]
		$Delimiter = " "
	)

	begin {
		$items = @()
	}

	process {
		$items += $words
	}

	end {
		return ($items -join $Delimiter)
	}
}

function Logging {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position = 0)]
		[string[]]
		$line,

		[Parameter(Position = 1)]
		[ref]
		$var,

		[switch]
		$stderr,

		[string]
		$file

	)

	if ($stderr) {
		$line | Write-StdErr
	} else {
		Write-Output $line
	}

	if ($var) {
		$OFS = [Environment]::NewLine
		$var.Value = $var.Value + $OFS + $line
	}

	if ($file) {
		try {
			if (! $(Test-Path -LiteralPath $(Split-Path $file -Parent))) {
				New-Item -ItemType Directory -Path $(Split-Path $file -Parent) | Out-Null
			}
			$line | Out-File -LiteralPath $file -Append -Force
			# -Encoding
		} catch {
			Write-ErrorObject $_ | Logging -var $var -stderr
		}
	}

}

function Format-FileSize() {
	param (
		$bytes,
		$precision = 2
	)

	foreach ($i in ("B","KB","MB","GB","TB")) {
		if (($bytes -lt 1000) -or ($i -eq "TB")) {
			$bytes = ($bytes).ToString("F0" + "$precision")
			return $bytes + " $i"
		} else {
			$bytes /= 1KB
		}
	}
}

function Test-BotToken {
<#
	.SYNOPSIS
		Validates Bot auth Token

	.DESCRIPTION
		A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		Test-BotToken -BotToken $botToken

		Validates the specified Bot auth token via Telegram API

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		Test-BotToken -BotToken $botToken -Verbose

		Validates the specified Bot auth token via Telegram API

	.PARAMETER BotToken
		Use this token to access the HTTP API

	.OUTPUTS
		System.Management.Automation.PSCustomObject (if successful)
		System.Boolean (on failure)

	.NOTES
		Author: Jake Morrison - @jakemorrison - https://techthoughts.info/
		This works with PowerShell Versions: 5.1, 6.0, 6.1
		For a description of the Bot API, see this page: https://core.telegram.org/bots/api
		How do I get my channel ID? Use the getidsbot https://telegram.me/getidsbot  -or-  Use the Telegram web client and copy the channel ID in the address
		How do I set up a bot and get a token? Use the BotFather https://t.me/BotFather

	.COMPONENT
		PoshGram - https://github.com/techthoughts2/PoshGram

	.FUNCTIONALITY
		A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.

	.LINK
		https://github.com/techthoughts2/PoshGram/blob/master/docs/Test-BotToken.md
		https://core.telegram.org/bots/api#getme
#>

	[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory = $true,
			HelpMessage = '#########:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$BotToken
	)

	$results = $true

	$invokeRestMethodSplat = @{
		Uri = ("https://api.telegram.org/bot{0}/getMe" -f $BotToken)
		ErrorAction = 'Stop'
		Method = 'Get'
	}

	try {
		Write-Verbose -Message "Testing Bot Token..."
		$results = Invoke-RestMethod @invokeRestMethodSplat
	}
	catch {
		Write-Warning "An error was encountered testing the BOT token:"
		Write-Error $_
		$results = $false
	}
	return $results
}

function Send-TelegramTextMessage {
<#
	.SYNOPSIS
		Sends Telegram text message via Bot API

	.DESCRIPTION
		Uses Telegram Bot API to send text message to specified Telegram chat. Several options can be specified to adjust message parameters.

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		$chat = "-nnnnnnnnn"
		Send-TelegramTextMessage -BotToken $botToken -ChatID $chat -Message "Hello"

	Sends text message via Telegram API

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		$chat = "-nnnnnnnnn"
		Send-TelegramTextMessage `
			-BotToken $botToken `
			-ChatID $chat `
			-Message "Hello *chat* _channel_, check out this link: [TechThoughts](https://techthoughts.info/)" `
			-ParseMode MarkdownV2 `
			-DisablePreview `
			-DisableNotification `
			-Verbose

	Sends text message via Telegram API

	.EXAMPLE
		$sendTelegramTextMessageSplat = @{
			BotToken = $botToken
			ChatID = $chat
			ParseMode = 'MarkdownV2'
			Message = 'This is how to escape an underscore in a message: \_'
		}
		Send-TelegramTextMessage @sendTelegramTextMessageSplat

	Sends text message via Telegram API using MarkdownV2 with a properly escaped character.

	.EXAMPLE
		$sendTelegramTextMessageSplat = @{
			BotToken = $botToken
			ChatID = $chat
			ParseMode = 'MarkdownV2'
			Message = 'You can underline __words__ in messages\.'
		}
		Send-TelegramTextMessage @sendTelegramTextMessageSplat

	Sends text message via Telegram API using MarkdownV2 with an underlined word and a properly escaped character.

	.PARAMETER BotToken
		Use this token to access the HTTP API

	.PARAMETER ChatID
		Unique identifier for the target chat

	.PARAMETER Message
		Text of the message to be sent

	.PARAMETER ParseMode
		Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in your bot's message. Default is HTML.

	.PARAMETER DisablePreview
		Disables link previews for links in this message.

	.PARAMETER DisableNotification
		Send the message silently. Users will receive a notification with no sound.

	.OUTPUTS
		System.Management.Automation.PSCustomObject (if successful)
		System.Boolean (on failure)

	.NOTES
		Author: Jake Morrison - @jakemorrison - https://techthoughts.info/
		This works with PowerShell Versions: 5.1, 6+, 7+
		For a description of the Bot API, see this page: https://core.telegram.org/bots/api
		How do I get my channel ID? Use the getidsbot https://telegram.me/getidsbot  -or-  Use the Telegram web client and copy the channel ID in the address
		How do I set up a bot and get a token? Use the BotFather https://t.me/BotFather

		Markdown Style: This is a legacy mode, retained for backward compatibility.
		When using Markdown/Markdownv2 you must properly escape characters.

	.COMPONENT
		PoshGram - https://github.com/techthoughts2/PoshGram

	.FUNCTIONALITY
		Parameters                  Type                Required    Description
		chat_id                     Integer or String   Yes         Unique identifier for the target chat or username of the target channel (in the format @channelusername)
		text                        String              Yes         Text of the message to be sent
		parse_mode                  String              Optional    Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in your bot's message.
		disable_web_page_preview    Boolean             Optional    Disables link previews for links in this message
		disable_notification        Boolean             Optional    Sends the message silently. Users will receive a notification with no sound.
		reply_to_message_id         Integer             Optional    If the message is a reply, ID of the original message

	.LINK
		https://github.com/techthoughts2/PoshGram/blob/master/docs/Send-TelegramTextMessage.md
		https://core.telegram.org/bots/api#sendmessage
		https://core.telegram.org/bots/api#html-style
		https://core.telegram.org/bots/api#markdownv2-style
		https://core.telegram.org/bots/api#markdown-style

	.ORIGINAL
		https://raw.githubusercontent.com/techthoughts2/PoshGram/master/src/PoshGram/Public/Send-TelegramTextMessage.ps1
#>

	[CmdletBinding()]
	Param (
		# you could set a token right here if you wanted
		[Parameter(Mandatory = $true, HelpMessage = '#########:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]
		$BotToken,
		# you could set a Chat ID right here if you wanted
		[Parameter(Mandatory = $true, HelpMessage = '-#########')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]
		$ChatID,
		# message
		[Parameter(Mandatory = $true, HelpMessage = 'Text of the message to be sent')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]
		$Message,
		# formatting (set to HTML by default)
		[Parameter(Mandatory = $false, HelpMessage = 'HTML vs Markdown for message formatting')]
		[ValidateSet('Markdown', 'MarkdownV2', 'HTML')]
		[string]
		$ParseMode = 'HTML',
		# link previews (set to false by default)
		[Parameter(Mandatory = $false, HelpMessage = 'Disables link previews')]
		[switch]
		$DisablePreview,
		# send the message silently
		[Parameter(Mandatory = $false, HelpMessage = 'Send the message silently')]
		[switch]
		$DisableNotification,
		# proxy
		[Parameter(Mandatory = $false, HelpMessage = 'Set proxy')]
		[string]
		$proxy
	)

	$results = $true

	$payload = @{
		chat_id = $ChatID
		text = $Message
		parse_mode = $ParseMode
		disable_web_page_preview = $DisablePreview.IsPresent
		disable_notification = $DisableNotification.IsPresent
	}

	$invokeRestMethodSplat = @{
		Uri = ("https://api.telegram.org/bot{0}/sendMessage" -f $BotToken)
		Body = ([System.Text.Encoding]::UTF8.GetBytes((ConvertTo-Json -Compress -InputObject $payload)))
		ErrorAction = 'Stop'
		ContentType = "application/json"
		Method = 'Post'
	}

	if ($proxy) {
		$invokeRestMethodSplat.Add('Proxy', $proxy)
	}

	try {
		$results = Invoke-RestMethod @invokeRestMethodSplat
	}
	catch {
		$results = $_
	}
	return $results
}

function Send-TelegramLocalPhoto {
<#
	.SYNOPSIS
		Sends Telegram photo message via Bot API from locally sourced photo image

	.DESCRIPTION
		Uses Telegram Bot API to send photo message to specified Telegram chat. The photo will be sourced from the local device and uploaded to telegram. Several options can be specified to adjust message parameters.

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		$chat = "-nnnnnnnnn"
		$photo = "C:\photos\aphoto.jpg"
		Send-TelegramLocalPhoto -BotToken $botToken -ChatID $chat -PhotoPath $photo

	Sends photo message via Telegram API

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		$chat = "-nnnnnnnnn"
		$photo = "C:\photos\aphoto.jpg"
		Send-TelegramLocalPhoto `
			-BotToken $botToken `
			-ChatID $chat `
			-PhotoPath $photo `
			-Caption "Check out this photo" `
			-ParseMode MarkdownV2 `
			-DisableNotification `
			-Verbose

	Sends photo message via Telegram API

	.EXAMPLE
		$botToken = "nnnnnnnnn:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx"
		$chat = "-nnnnnnnnn"
		$photo = "C:\photos\aphoto.jpg"
		$sendTelegramLocalPhotoSplat = @{
			BotToken  = $botToken
			ChatID    = $chat
			PhotoPath = $photo
			Caption   = "Check out this __awesome__ photo\."
			ParseMode = 'MarkdownV2'
		}
		Send-TelegramLocalPhoto @sendTelegramLocalPhotoSplat

	Sends photo message via Telegram API with properly formatted underlined word and escaped special character.

	.PARAMETER BotToken
		Use this token to access the HTTP API

	.PARAMETER ChatID
		Unique identifier for the target chat

	.PARAMETER PhotoPath
		File path to local image

	.PARAMETER Caption
		Brief title or explanation for media

	.PARAMETER ParseMode
		Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in your bot's message. Default is HTML.

	.PARAMETER DisableNotification
		Send the message silently. Users will receive a notification with no sound.

	.OUTPUTS
		System.Management.Automation.PSCustomObject (if successful)
		System.Boolean (on failure)

	.NOTES
		Author: Jake Morrison - @jakemorrison - https://techthoughts.info/
		This works with PowerShell Version: 6.1+

		The following photo types are supported:
		JPG, JPEG, PNG, GIF, BMP, WEBP, SVG, TIFF

		For a description of the Bot API, see this page: https://core.telegram.org/bots/api
		How do I get my channel ID? Use the getidsbot https://telegram.me/getidsbot  -or-  Use the Telegram web client and copy the channel ID in the address
		How do I set up a bot and get a token? Use the BotFather https://t.me/BotFather

	.COMPONENT
		PoshGram - https://github.com/techthoughts2/PoshGram

	.FUNCTIONALITY
		Parameters              Type                    Required    Description
		chat_id                 Integer or String       Yes         Unique identifier for the target chat or username of the target channel (in the format @channelusername)
		photo                   InputFile or String     Yes         Photo to send. Pass a file_id as String to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. More info on Sending Files
		caption                 String                  Optional    Photo caption (may also be used when resending photos by file_id), 0-200 characters
		parse_mode              String                  Optional    Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in the media caption.
		disable_notification    Boolean                 Optional    Sends the message silently. Users will receive a notification with no sound.

	.LINK
		https://github.com/techthoughts2/PoshGram/blob/master/docs/Send-TelegramLocalPhoto.md
		https://core.telegram.org/bots/api#sendphoto
		https://core.telegram.org/bots/api#html-style
		https://core.telegram.org/bots/api#markdownv2-style
		https://core.telegram.org/bots/api#markdown-style
#>

	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true,
			HelpMessage = '#########:xxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxx')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$BotToken, #you could set a token right here if you wanted
		[Parameter(Mandatory = $true,
			HelpMessage = '-#########')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$ChatID, #you could set a Chat ID right here if you wanted
		[Parameter(Mandatory = $true,
			HelpMessage = 'File path to the photo you wish to send')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$PhotoPath,
		[Parameter(Mandatory = $false,
			HelpMessage = 'Photo caption')]
		[string]$Caption = "", #set to false by default
		[Parameter(Mandatory = $false,
			HelpMessage = 'HTML vs Markdown for message formatting')]
		[ValidateSet('Markdown', 'MarkdownV2', 'HTML')]
		[string]$ParseMode = 'HTML', #set to HTML by default
		[Parameter(Mandatory = $false,
			HelpMessage = 'Send the message silently')]
		[switch]$DisableNotification
	)

	$results = $true

	Write-Verbose -Message "Verifying presence of photo..."
	if (!(Test-Path -Path $PhotoPath)) {
		Write-Warning "The specified photo path: $PhotoPath was not found."
		$results = $false
		return $results
	}
	else {
		Write-Verbose -Message "Path verified."
	}

	Write-Verbose -Message "Verifying extension type..."
	$fileTypeEval = Test-FileExtension -FilePath $PhotoPath -Type Photo
	if ($fileTypeEval -eq $false) {
		$results = $false
		return $results
	}
	else {
		Write-Verbose -Message "Extension supported."
	}

	Write-Verbose -Message "Verifying file size..."
	$fileSizeEval = Test-FileSize -Path $PhotoPath
	if ($fileSizeEval -eq $false) {
		$results = $false
		return $results
	}
	else {
		Write-Verbose -Message "File size verified."
	}

	try {
		$fileObject = Get-Item $PhotoPath -ErrorAction Stop
	}
	catch {
		Write-Warning "The specified photo could not be interpreted properly."
		$results = $false
		return $results
	}

	$uri = "https://api.telegram.org/bot$BotToken/sendphoto"

	$Form = @{
		chat_id = $ChatID
		photo = $fileObject
		caption = $Caption
		parse_mode = $ParseMode
		disable_notification = $DisableNotification.IsPresent
	}

	$invokeRestMethodSplat = @{
		Uri = $Uri
		ErrorAction = 'Stop'
		Form = $Form
		Method = 'Post'
	}

	try {
		$results = Invoke-RestMethod @invokeRestMethodSplat
	}
	catch {
		Write-Warning "An error was encountered sending the Telegram photo message:"
		Write-Error $_
		$results = $false
	}
	return $results
}

function Test-FileExtension {
<#
	.SYNOPSIS
		Verifies that specified file is a supported Telegram extension type

	.DESCRIPTION
		Evaluates the specified file path to determine if the file is a supported extension type

	.EXAMPLE
		Test-FileExtension -FilePath C:\photos\aphoto.jpg -Type Photo

		Verifies if the path specified is a supported photo extension type

	.EXAMPLE
		Test-FileExtension -FilePath $PhotoPath -Type Photo -Verbose

		Verifies if the path specified in $PhotoPath is a supported photo extension type with verbose output
	.EXAMPLE
		$fileTypeEval = Test-FileExtension -FilePath $AnimationPath -Type Animation

		Verifies if the path specified is a supported Animation extension type
	.EXAMPLE
		$fileTypeEval = Test-FileExtension -FilePath $Audio -Type Audio

		Verifies if the path specified is a supported Audio extension type
	.EXAMPLE
		$fileTypeEval = Test-FileExtension -FilePath $PhotoPath -Type Photo

		Verifies if the path specified is a supported photo extension type
	.EXAMPLE
		$fileTypeEval = Test-FileExtension -FilePath $Video -Type Video

		Verifies if the path specified is a supported Video extension type
	.EXAMPLE
		$fileTypeEval = Test-FileExtension -FilePath $Sticker -Type Sticker

		Verifies if the path specified is a supported Sticker extension type

	.PARAMETER FilePath
		Path to file that will be evaluated

	.PARAMETER Type
		Telegram message type

	.OUTPUTS
		System.Boolean

	.NOTES
		Author: Jake Morrison - @jakemorrison - https://techthoughts.info/

	.COMPONENT
		PoshGram - https://github.com/techthoughts2/PoshGram
#>

	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true,
			HelpMessage = 'Path to file that will be evaluated')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$FilePath,
		[Parameter(Mandatory = $true,
			HelpMessage = 'Telegram message type')]
		[ValidateSet('Photo', 'Video', 'Audio', 'Animation', 'Sticker')]
		[string]$Type
	)

	$supportedPhotoExtensions = @(
		'JPG',
		'JPEG',
		'PNG',
		'GIF',
		'BMP',
		'WEBP',
		'SVG',
		'TIFF'
	)
	$supportedVideoExtensions = @(
		'MP4'
	)
	$supportedAudioExtensions = @(
		'MP3'
	)
	$supportedAnimationExtensions = @(
		'GIF'
	)
	$supportedStickerExtensions = @(
		'WEBP',
		'TGS'
	)
	switch ($Type) {
		Photo {
			$extType = $supportedPhotoExtensions
		}
		Video {
			$extType = $supportedVideoExtensions
		}
		Audio {
			$extType = $supportedAudioExtensions
		}
		Animation {
			$extType = $supportedAnimationExtensions
		}
		Sticker {
			$extType = $supportedStickerExtensions
		}
	}
	Write-Verbose -Message "Validating type: $Type"

	[bool]$results = $true #assume the best.

	Write-Verbose -Message "Processing $FilePath ..."
	$divide = $FilePath.Split(".")
	$rawExtension = $divide[$divide.Length - 1]
	$extension = $rawExtension.ToUpper()
	Write-Verbose "Verifying discovered extension: $extension"
	switch ($extension) {
		{ $extType -contains $_ } {
			Write-Verbose -Message "Extension verified."
		}
		default {
			Write-Warning -Message "The specified file is not a supported $Type extension."
			$results = $false
		}
	}
	return $results
}

function Test-FileSize {
<#
	.SYNOPSIS
		Verifies that file size is supported by Telegram

	.DESCRIPTION
		Evaluates if the file is at or below the supported Telegram file size

	.EXAMPLE
		Test-FileSize -Path C:\videos\video.mp4

		Verifies if the path specified is a supported video extension type

	.EXAMPLE
		Test-FileSize -Path $Path -Verbose

		Verifies if the path specified in $VideoPath is a supported video extension type with verbose output

	.PARAMETER Path
		Path to file

	.OUTPUTS
		System.Boolean

	.NOTES
		Author: Jake Morrison - @jakemorrison - https://techthoughts.info/
		Telegram currently supports a 50MB file size for bots

	.COMPONENT
		PoshGram - https://github.com/techthoughts2/PoshGram
#>

	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true,
			HelpMessage = 'Path to file')]
		[ValidateNotNull()]
		[ValidateNotNullOrEmpty()]
		[string]$Path
	)

	$results = $true
	$supportedSize = 50

	try {
		$size = Get-ChildItem -Path $Path -ErrorAction Stop
		if (($size.Length / 1MB) -gt $supportedSize) {
			Write-Warning -Message "The file is over $supportedSize (MB)"
			$results = $false
		}
	}
	catch {
		Write-Warning -Message "An error was encountered evaluating the file size"
		$results = $false
	}
	return $results
}

function Copy-ModifiedFile {
	[CmdletBinding()]
	param (
		# Path from source file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path from source file.")]
		[Alias("s", "src")]
		[ValidateNotNullOrEmpty()]
		[string]
		$srcfile,
		# Path to destination file
		[Parameter(
			Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to destination file.")]
		[Alias("d", "dst")]
		[ValidateNotNullOrEmpty()]
		[string]
		$dstfile
	)

	# $fileName = ''
	# $file = [System.io.File]::Open($fileName, 'Open', 'Read', 'None'); Write-Host "Press any key to continue ..."; $null = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown"); $file.Close()

	try {
		if (! $(Test-Path -PathType Leaf -LiteralPath $dstfile)) {
			if (! $(Test-Path -PathType Container -LiteralPath $(Split-Path -Parent -Path $dstfile))) {
				New-Item -Force -ItemType Directory -Path $(Split-Path -Parent -Path $dstfile) | Out-Null
			}
			Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
			return $true
		} else {
			if ($(Get-FileHash -LiteralPath $srcfile).Hash -ne $(Get-FileHash -LiteralPath $dstfile).Hash) {
				Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
				return $true
			}
		}
		return $false
	}
	catch {
		return $_
	}
}

function Write-ErrorObject {
<#
	.SYNOPSIS
		Return string from ErrorRecord object.
#>

	param ([PSObject] $InputObject)

	begin {
		$line = @()
	}

	process {
		$line += 'Error'

		if ($InputObject.InvocationInfo.MyCommand) {
			$line += $InputObject.InvocationInfo.MyCommand.ToString()
		}

		$line += ':'

		if ($InputObject.Exception.Message) {
			$line += $InputObject.Exception.Message.ToString().Trim()
		}

		if ($InputObject.InvocationInfo.ScriptLineNumber) {
			$line += 'line'
			$line += $InputObject.InvocationInfo.ScriptLineNumber.ToString()
		}
	}

	end {
		return ($line -join ' ')
	}
}

function Test-SMTP {
<#
	.SYNOPSIS
		Test SMTP connect.

	.EXAMPLE
		Test-SMTP -smtp 'mail.domain.ex' -port 25
		Test-SMTP -h 'mail.domain.ex'
		Test-SMTP 'mail.domain.ex'

	.OUTPUTS
		$true or [System.Management.Automation.ErrorRecord]
#>

	param (
		# SMTP Host
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position=0)]
		[Alias('h')]
		[string[]]
		$smtp,
		# SMTP Port
		[Alias('p')]
		[int]
		$port = 25
	)

	try {
		$object = New-Object -TypeName System.Net.Sockets.TcpClient -ArgumentList $smtp,$port
		$r = $object.Connected
		$object.Close()
	}
	catch {
		return $_
	}
	return $r
}

function Send-Mail {
<#
	.SYNOPSIS
		Send mail

	.OUTPUTS
		$true or [System.Management.Automation.ErrorRecord]
#>

<#
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/send-mailmessage?view=powershell-7

Send-MailMessage
	[-Attachments <String[]>]
	[-Bcc <String[]>]
	[[-Body] <String>]
	[-BodyAsHtml]
	[-Encoding <Encoding>]
	[-Cc <String[]>]
	[-DeliveryNotificationOption <DeliveryNotificationOptions>]
	-From <String>
	[[-SmtpServer] <String>]
	[-Priority <MailPriority>]
	[-ReplyTo <String[]>]
	[[-Subject] <String>]
	[-To] <String[]>
	[-Credential <PSCredential>]
	[-UseSsl]
	[-Port <Int32>]
	[<CommonParameters>]
#>

<#
	https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_advanced_methods
	https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_cmdletbindingattribute
	https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_advanced_parameters
	https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_outputtypeattribute
	https://ss64.com/ps/syntax-datatypes.html
#>

	[CmdletBinding()]
	param (
		[Parameter(Mandatory = $true)]
		[Alias('s')]
		[string]
		$smtp,

		[Parameter()]
		[Alias('p')]
		[int]
		$port = 25,

		[Parameter(Mandatory = $true)]
		[string]
		$from,

		[Parameter(Mandatory = $true)]
		[string[]]
		$to,

		[Parameter()]
		[Alias('subject')]
		[string]
		$subj,

		[Parameter(Mandatory = $true, ValueFromPipeline = $true)]
		[Alias('b')]
		[string]
		$body,

		[Parameter()]
		# https://docs.microsoft.com/en-us/dotnet/api/system.text.encoding
		[string]
		$encoding = 'utf8'
	)

	try {
		if ($PSVersionTable.PSVersion.Major -gt 2) {
			Send-MailMessage -SmtpServer $smtp -Port $port -UseSsl -From $from -To $to -Subject $subj -Body $body -Encoding $encoding -ErrorAction Stop
		} else {
			Send-MailMessage -SmtpServer $smtp -UseSsl -From $from -To $to -Subject $subj -Body $body -ErrorAction Stop
		}
		return $true
	}
	catch {
		return $_
	}
}

function Get-ExitCodeDescription7z {
	param ([int] $exitcode)

	if (! $exitcode) {
		return $null
	}

	# https://sourceforge.net/p/sevenzip/discussion/45797/thread/c374fd35/#f956/3d2c/d109
	switch ($exitcode) {
		0 {return 'Successful operation'}
		1 {return 'Non fatal error(s) occurred'}
		2 {return 'A fatal error occurred'}
		3 {return 'A CRC error occurred when unpacking'}
		4 {return 'Attempt to modify an archive previously locked'}
		5 {return 'Write to disk error'}
		6 {return 'Open file error'}
		7 {return 'Command line option error'}
		8 {return 'Not enough memory for operation'}
		9 {return 'Create file error'}
		255 {return 'User stopped the process'}
		Default {return $null}
	}
}

function IsDirectory {
	[CmdletBinding()]
	param (
		[Parameter(
			Mandatory=$true,
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$path
	)

	try {
		if ((Get-Item -Force -LiteralPath $path -ErrorAction Stop) -is [System.IO.DirectoryInfo]) {
			return $true
		} else {
			return $false
		}
	}
	catch {
		return $_
	}
}
