# Pwsh common

Common functions for pwsh.

## Adding a repository as a submodule

Добавление репозитория в качестве субмодуля

```shell
git submodule add -- {url repo} {relative path}
```
